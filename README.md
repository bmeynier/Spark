# Basic Spark appplication

## How install Spark local ?
```
curl -s "https://get.sdkman.io" | bash
sdk install java 11.0.11.hs-adpt
ssdk install scala 2.13.5
sdk install sbt
sdk install spark
```

## How to compile ?
```
sbt clean package
```

## How to launch ?
```
spark-submit target/scala-2.12/spark-demo_2.12-0.1.jar ./src/main/resources/ventreDeParis.txt
```

### This project is linked with the following [Blog Post](http://baptiste-meynier.com/index.php/2021/09/02/spark/)