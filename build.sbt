name := "spark-demo"

version := "0.1"

scalaVersion := "2.12.14"

val sparkVersion = "2.4.4"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion % "provided"
  , "org.apache.spark" %% "spark-sql"   % sparkVersion
)

mainClass := Some("com.bmeynier.article.MeanLength")

idePackagePrefix := Some("com.bmeynier.article.spark.intro")
