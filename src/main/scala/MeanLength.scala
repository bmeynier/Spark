package com.bmeynier.article.spark.demo

import org.apache.spark._

object MeanLength {
  def main(args: Array[String]): Unit = {
    val textToAnalysePath = args(0)
    val conf = new SparkConf().setAppName("mean-length")
    val sc = new SparkContext(conf)
    val entry = sc.textFile(textToAnalysePath)

    val words = entry.flatMap(l => l.split(" "))
    val rdd_count_words = words.map(_ => ("WORDS", 1)).reduceByKey { case (x, y) => x + y }
    val rdd_count_letters = words.map(m => ("LETTERS", m.length())).reduceByKey(_ + _)

    val letters_count = rdd_count_letters.collect()(0)._2
    val words_count = rdd_count_words.collect()(0)._2
    val means = letters_count / words_count.toFloat
    println("The words mean is:" + means)
  }
}
